const express = require("express");
const socketio = require("socket.io");
const http = require("http");
const PORT = 8087;
const cors = require("cors");
const app = express();
const server = http.createServer(app);
const config = require("./config.js");
const io = socketio(server, {
  cors: {
    origin: `${config.developer}`,
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true,
  },
});
app.use(
  cors({
    origin: `${config.developer}`,
    credentials: true,
  })
);

io.on("connection", (socket) => {
  socket.on("join", (data) => {
    io.emit("response", data);
  });
  socket.on("order", (data) => {
    io.emit("response", data);
  });
});

server.listen(PORT, () => {
  console.log("Connected Successfully");
});
